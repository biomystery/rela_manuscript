#!/bin/bash

#step1 computeMatrix
computeMatrix scale-regions -b 500 -a 500 -R roberto-20150625-all_peaks.bed -S /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Input-Wt-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Input-Wt-R2.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Wt-0-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Wt-0-R2.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Wt-30-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Wt-30-R2.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Input-DBDmt-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/Input-DBDmt-R2.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/DBDmt-0-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/DBDmt-0-R2.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/DBDmt-30-R1.bw /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch1/07_tracks/DBDmt-30-R2.bw --skipZeros -o matrix.gz --outFileNameMatrix matrix.tab --outFileSortedRegions regions_multiple_genes.bed


#step2 plot heatmap 
plotHeatmap  -m matrix.gz -out hm-cluster-8.png --kmeans 8 --startLabel "5'" --endLabel "3'" --xAxisLabel '' --yAxisLabel '' --outFileSortedRegions hm-cluster.bed --outFileNameMatrix hm-cluster.tab
plotHeatmap  -m matrix.gz -out hm.png --startLabel "5'" --endLabel "3'" --xAxisLabel '' --yAxisLabel ''

# find  /mnt/biggie/backed_up/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks -name '[0-9]*.bw' -exec bash plotCoverage.sh -b 500 -a 500 -R ./roberto-20150625-all_peaks.bed -S {} + 

