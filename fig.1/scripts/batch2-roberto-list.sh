#!/bin/bash

#step1 computeMatrix
computeMatrix scale-regions -b 500 -a 500 -R ./roberto-20150625-all_peaks.bed -S /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/Input-R1.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/Input-R2.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/0-R1.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/0-R2.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/30m-R1.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/30m-R2.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/3H-R1.bw /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks/3H-R2.bw --skipZeros -o matrix.gz --outFileNameMatrix matrix.tab --outFileSortedRegions regions_multiple_genes.bed


#step2 plot heatmap 
plotHeatmap  -m matrix.gz -out hm-clust4.png --kmeans 4 --startLabel "5'" --endLabel "3'" --xAxisLabel '' --yAxisLabel '' --outFileSortedRegions hm-cluster.bed --outFileNameMatrix hm-cluster.tab

plotHeatmap  -m matrix.gz -out hm.png --startLabel "5'" --endLabel "3'" --xAxisLabel '' --yAxisLabel '' --whatToShow 'plot and heatmap'

 find  /mnt/biggie/data1/roberto/analyses/Kim/p65_ChIP/batch2/07_tracks -name '[^S]*.bw' -exec bash plotCoverage -b 500 -a 500 -R ./roberto-20150625-all_peaks.bed -S {} + 

