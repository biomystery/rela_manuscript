
#!/usr/bin/env Rscript 
args = commandArgs(trailingOnly=TRUE)

# test if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} 

par.windowsize <- as.numeric(args[1])
do.plot <- as.logical(args[2])
# read peak bed  ----------------------------------------------------
#peak.bed.raw <- read.delim(file = './data/roberto-20150625-all_peaks.bed',header = F,
#                           stringsAsFactors = F)
peak.bed.raw <- read.csv(file = './motif.rela.peak.res.csv',header = T,
                           stringsAsFactors = F)

#colnames(peak.bed.raw) <- c("Chr",'Start',"End")

genes.anno <- read.delim(file='~/Dropbox/Projects/RelA_project/data/2016-01-29-RNASeq-Rec-b2/counts.txt',stringsAsFactors = F)
genes.anno <- genes.anno[,1:6]; genes.anno$Geneid <- substr(genes.anno$Geneid,1,18)
rownames(genes.anno) <- genes.anno$Geneid; genes.anno$Geneid <- NULL 

ev<- new.env();load(file = "./data/glist.RData",ev)

getTS <- function(anno,type='TSS'){
  s <- anno$Strand
  mutlipleS<- ifelse(type=='TSS',anno$Start,anno$End) 
  tmp <- unlist(strsplit(mutlipleS,split = ';'))
  ifelse(s=='+',tmp[1],tmp[length(tmp)])
}

rela.genes <- unlist(ev$g.list.combined[1:2])
rela.genes.anno <- genes.anno[rela.genes,]

rela.genes.anno$Strand <- unlist(lapply(rela.genes.anno$Strand,
                                        function(x) unlist(strsplit(x,split = ';'))[1]))
rela.genes.anno$TSS <- sapply(1:nrow(rela.genes.anno),
                              function(x) as.numeric(getTS(rela.genes.anno[x,],type='TSS')))
rela.genes.anno$Chr <- unlist(lapply(rela.genes.anno$Chr,
                                     function(x) unlist(strsplit(x,split = ";"))[1]))


# reduce the bed 
chr.common <- intersect(unique(rela.genes.anno$Chr),unique(peak.bed.raw$Chr))
peak.bed.raw <- subset(peak.bed.raw, Chr %in% chr.common)
peak.bed.raw$length <- peak.bed.raw$End -  peak.bed.raw$Start + 1
all(peak.bed.raw$length>0)

# define comparason parameters 
getPeakMat <-function(par.windowsize = 10000,debug=F){
  rela.peak.mat <- matrix(0,nrow = nrow(rela.genes.anno),
                          ncol = par.windowsize*2+1)
  rela.peak.anno <- data.frame()
  rela.gene.region <- data.frame(Chr= rela.genes.anno$Chr,
                                 Start = rela.genes.anno$TSS - par.windowsize,
                                 End = rela.genes.anno$TSS+ par.windowsize)
  
  rownames(rela.gene.region)<-rownames(rela.peak.mat) <- rela.genes 
  for(i in 1:nrow(peak.bed.raw)){
    #i <- 1  
    peak.i <- peak.bed.raw[i,]
    
    # first match chr 
    is.match.chr <- rela.gene.region$Chr == peak.i$Chr #length of 563
    peak.i.d_ps_s <- peak.i$Start - rela.gene.region$Start #length of 563
    peak.i.d_pe_s <- peak.i$End - rela.gene.region$Start #length of 563
    peak.i.d_ps_e <- peak.i$Start - rela.gene.region$End #length of 563
    idx.criteria.1 <- which(peak.i.d_ps_s<=0 & peak.i.d_pe_s>=0 & is.match.chr)
    idx.criteria.2 <- which(peak.i.d_ps_s>=0 & peak.i.d_ps_e<=0 & is.match.chr)
    
    if(length(idx.criteria.1)>0){
      # found indexes/genes for criteria 1
      for(g_id in idx.criteria.1){
        end.id <- min(peak.i$length+peak.i.d_ps_s[g_id],
                      ncol(rela.peak.mat)) 
        overlap.id <- 1:end.id
        rela.peak.mat[g_id,overlap.id] <- rela.peak.mat[g_id,overlap.id]+peak.i$zval
      }
    }else if(length(idx.criteria.2)>0) {
      # found indexes/genes for criteria 2
      for(g_id in idx.criteria.2){
        end.id <- min(peak.i.d_ps_s[g_id]+peak.i$length,ncol(rela.peak.mat))
        overlap.id <- (peak.i.d_ps_s[g_id]+1):end.id
        if(debug) print(data.frame(ps_s=peak.i.d_ps_s[g_id],
                                   ps_e=peak.i.d_ps_e[g_id],
                                   g_id = g_id,
                                   peak.i,
                                   rela.gene.region[g_id,]))
        
        rela.peak.mat[g_id,overlap.id] <- rela.peak.mat[g_id,overlap.id]+peak.i$zval
      }}}
  rela.peak.mat  
}

system.time(rela.peak.mat <- getPeakMat(par.windowsize = par.windowsize))

## change for the plotting 
rela.peak.mat[rela.peak.mat==0] <- -1 
rela.peak.mat[is.na(rela.peak.mat)] <- 0 

# plot the results  -------------------------------------------------------
if(do.plot){
cols <- colorRampPalette(c("gold",'darkmagenta'))(12)
cols <- c("grey90","darkolivegreen2",cols)
barplot(rep(1,length(cols)),pch=16,col=cols,space = 0)
png(file=paste0("rela.peak.mat.",par.windowsize,'.png'))
require(pheatmap)
pheatmap(rela.peak.mat[gorder,],scale = 'none',
         show_rownames = F,
         #color = c("grey90","darkmagenta"),
         breaks = seq(-1,13),
         color = cols,gaps_row = ggap,
         show_colnames = F,
         cluster_rows = F,cluster_cols = F,legend = T,
         gaps_col = (ncol(rela.peak.mat-1)/2))
dev.off()
}
write.csv(file=paste0('rela.peak.mat.',par.windowsize,'.csv')
          ,rela.peak.mat)

pd <- rela.peak.mat[gorder,]
pd.reduce <- apply(pd,1,function(x){
  x.new <- rep(0,2001)
  x.new[1001] <- x[10001]
  for(i in 1:1000)
    x.new[i] <- max(x[((i-1)*10+1):(i*10)])
  for(i in 1001:2000)
    x.new[i] <- max(x[((i-1)*10+2):(i*10+1)])
  x.new
})
pd.reduce <- t(pd.reduce)



# testing-code ----------------------------------------------------------------
# check matchs 
if(F){
  which(rowSums(rela.peak.mat)>1)
  (peak.i <- peak.bed.raw["9824",] )
  peak.i.d_ps_e.check <- peak.i$Start - tmp.gene$End
  peak.i.d_pe_s.check <- peak.i$End - tmp.gene$Start
  peak.i.d_ps_s.check <- peak.i$Start - tmp.gene$Start
  peak.i.d_ps_s <- peak.i$Start - rela.gene.region$Start #length of 563
  peak.i.d_pe_s <- peak.i$End - rela.gene.region$Start #length of 563
  peak.i.d_ps_e <- peak.i$Start - rela.gene.region$End #length of 563
  idx.criteria.1 <- which(peak.i.d_ps_s<=0 & peak.i.d_pe_s>=0 & is.match.chr)
  idx.criteria.2 <- which(peak.i.d_ps_s>=0 & peak.i.d_ps_e<=0 & is.match.chr)
  
  c(peak.i.d_ps_s.check,peak.i.d_ps_e.check,peak.i.d_pe_s.check) # match 
  # -105 -4105   664 (type 1 match)
  which(peak.i.d_ps_s.check<=0 & peak.i.d_pe_s>=0 & is.match.chr)
  
}

